package br.com.railsonrsa.runrun_it.model.tasks;

import br.com.railsonrsa.runrun_it.data.Task;
import br.com.railsonrsa.runrun_it.model.RestFactory;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by railsonrsa on 29/06/17.
 */

public class PauseTaskObservable implements ObservableOnSubscribe<Boolean> {

    private int mTaskId;

    public PauseTaskObservable(int taskId) {
        mTaskId = taskId;
    }

    @Override
    public void subscribe(@NonNull ObservableEmitter<Boolean> e) throws Exception {
        RestFactory restFactory = new RestFactory();
        restFactory.setLoggingLevel(HttpLoggingInterceptor.Level.BASIC);
        TaskApi taskApi = restFactory.create(TaskApi.class);

        Call<Task> call = taskApi.pauseTask(mTaskId);
        Response<Task> response;

        try {
            response = call.execute();
            if (response.isSuccessful()){
                e.onNext(true);
            } else {
                e.onError(new Exception(response.message()));
            }
        } catch (Exception e1) {
            e.onError(e1);
        }
        e.onComplete();
    }
}
