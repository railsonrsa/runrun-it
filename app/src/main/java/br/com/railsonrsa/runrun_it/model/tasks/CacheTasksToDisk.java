package br.com.railsonrsa.runrun_it.model.tasks;

import java.util.List;

import br.com.railsonrsa.runrun_it.data.Task;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import io.realm.Realm;

/**
 * Created by railsonrsa on 29/06/17.
 */

class CacheTasksToDisk implements ObservableOnSubscribe<List<Task>> {

    private List<Task> mTasks;

    public CacheTasksToDisk(List<Task> tasks) {
        mTasks = tasks;
    }

    @Override
    public void subscribe(@NonNull ObservableEmitter<List<Task>> e) throws Exception {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        realm.copyToRealmOrUpdate(mTasks);

        realm.commitTransaction();
        e.onComplete();
    }
}
