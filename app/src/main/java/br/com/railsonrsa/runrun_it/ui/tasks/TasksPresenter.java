package br.com.railsonrsa.runrun_it.ui.tasks;

import android.util.Log;
import android.widget.ImageButton;

import java.util.List;

import br.com.railsonrsa.runrun_it.data.Task;
import br.com.railsonrsa.runrun_it.model.tasks.TaskModel;
import br.com.railsonrsa.runrun_it.ui.tasks.contracts.ActivityView;
import br.com.railsonrsa.runrun_it.ui.tasks.contracts.Presenter;
import br.com.railsonrsa.runrun_it.ui.tasks.contracts.TasksView;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Created by railsonrsa on 29/06/17.
 */

public class TasksPresenter implements Presenter {

    private final String TAG = "TasksPresenter";

    private ActivityView mActivityView;
    private TasksView mTasksView;
    private List<Task> mCachedTasks;

    public TasksPresenter(ActivityView activityView,
                          TasksView tasksView) {

        this.mActivityView = activityView;
        this.mTasksView = tasksView;

        mActivityView.setPresenter(this);
        mTasksView.setPresenter(this);
    }

    @Override
    public void start() {
        loadTasks();
    }

    @Override
    public void stop() {

    }

    @Override
    public void loadTasks() {
        TaskModel.getInstance()
                .loadTasks()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Task>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Task> tasks) {
                        Log.e(TAG, tasks.toString());
                        if (mCachedTasks != null) {
                            mTasksView.clearList();
                        }
                        mTasksView.updateList(tasks);
                        mCachedTasks = tasks;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void starOrPauseTask(final ImageButton taskStarOrStopButton, Task task) {
        if (task.isWorkingOn()) {
            TaskModel.getInstance().pauseTask(task.getId())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Boolean success) {
                            if (success) loadTasks();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {

            TaskModel.getInstance().playTask(task.getId())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Boolean success) {
                            if (success) loadTasks();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }

    @Override
    public void finishTask(ImageButton taskFinishButton, Task task) {
        TaskModel.getInstance().closeTask(task.getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean success) {
                        if (success) loadTasks();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
