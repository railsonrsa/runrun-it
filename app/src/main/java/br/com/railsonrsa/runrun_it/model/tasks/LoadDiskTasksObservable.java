package br.com.railsonrsa.runrun_it.model.tasks;

import android.util.Log;

import java.util.List;

import br.com.railsonrsa.runrun_it.data.Task;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by railsonrsa on 29/06/17.
 */

class LoadDiskTasksObservable implements ObservableOnSubscribe<List<Task>> {
    @Override
    public void subscribe(@NonNull ObservableEmitter<List<Task>> e) throws Exception {
        Log.e("Observable ->", "LoadDiskTasksObservable called!");

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<Task> tasks = realm.where(Task.class)
                .findAll()
                .sort("mQueuePosition", Sort.ASCENDING);

        List<Task> tasksList = realm.copyFromRealm(tasks);

        realm.commitTransaction();

        if (tasksList.size() > 0) {
            e.onNext(tasksList);
        }
        e.onComplete();
    }
}
