package br.com.railsonrsa.runrun_it.ui.tasks.contracts;

import java.util.List;

import br.com.railsonrsa.runrun_it.data.Task;
import br.com.railsonrsa.runrun_it.ui.base.BaseFragment;

/**
 * Created by railsonrsa on 29/06/17.
 */

public abstract class TasksView extends BaseFragment<Presenter>{
    public abstract void updateList(List<Task> tasks);

    public abstract void clearList();
}
