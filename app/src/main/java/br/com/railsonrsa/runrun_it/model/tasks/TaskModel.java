package br.com.railsonrsa.runrun_it.model.tasks;

import java.util.List;

import br.com.railsonrsa.runrun_it.data.Task;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by railsonrsa on 29/06/17.
 */

public final class TaskModel {

    private static TaskModel self;

    public static TaskModel getInstance() {
        if (self == null) {
            self = new TaskModel();
        }
        return self;
    }

    public Observable<List<Task>> loadTasks() {

        Observable<List<Task>> loadDiskTasks = Observable.create(new LoadDiskTasksObservable());
        Observable<List<Task>> loadCloudTasks = Observable.create(new LoadCloudTasksObservable());

        return Observable.concat(loadDiskTasks, loadCloudTasks).subscribeOn(Schedulers.io());
    }

    public Observable<List<Task>> cacheToDisk(List<Task> tasks) {
        return Observable.create(new CacheTasksToDisk(tasks)).subscribeOn(Schedulers.computation());
    }

    public Observable<Boolean> playTask(int taskId) {
        return Observable.create(new PlayTaskObservable(taskId)).subscribeOn(Schedulers.io());
    }

    public Observable<Boolean> pauseTask(int taskId) {
        return Observable.create(new PauseTaskObservable(taskId)).subscribeOn(Schedulers.io());
    }

    public Observable<Boolean> closeTask(int taskId) {
        return Observable.create(new CloseTaskObservable(taskId)).subscribeOn(Schedulers.io());
    }

}
