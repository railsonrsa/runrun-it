package br.com.railsonrsa.runrun_it.ui.base;

/**
 * Created by railsonrsa on 29/06/17.
 */

public interface BaseView<P> {
    void setPresenter(P presenter);
}
