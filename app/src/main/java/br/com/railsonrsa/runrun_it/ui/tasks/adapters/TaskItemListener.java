package br.com.railsonrsa.runrun_it.ui.tasks.adapters;

import android.widget.ImageButton;

import br.com.railsonrsa.runrun_it.data.Task;

/**
 * Created by railsonrsa on 29/06/17.
 */

public interface TaskItemListener {
    void onClickPlayOrStopButton(ImageButton taskStartStopButton, Task task);

    void onClickFinishButton(ImageButton taskFinishButton, Task task);
}
