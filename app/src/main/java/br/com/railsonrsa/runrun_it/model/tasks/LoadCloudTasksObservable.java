package br.com.railsonrsa.runrun_it.model.tasks;

import android.util.Log;

import java.util.List;

import br.com.railsonrsa.runrun_it.data.Task;
import br.com.railsonrsa.runrun_it.model.RestFactory;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by railsonrsa on 29/06/17.
 */

public class LoadCloudTasksObservable implements ObservableOnSubscribe<List<Task>> {
    @Override
    public void subscribe(@NonNull ObservableEmitter<List<Task>> e) throws Exception {
        Log.e("Observable ->", "LoadDiskTasksObservable called!");

        RestFactory restFactory = new RestFactory();
        restFactory.setLoggingLevel(HttpLoggingInterceptor.Level.BODY);
        TaskApi taskApi = restFactory.create(TaskApi.class);

        Call<List<Task>> call = taskApi.loadTasks("railson-santos-de-almeida");
        Response<List<Task>> response;

        try {
            response = call.execute();
            if (response.isSuccessful()) {
                TaskModel.getInstance()
                        .cacheToDisk(response.body())
                        .subscribe(new Observer<List<Task>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(List<Task> tasks) {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        });


                e.onNext(response.body());
            } else {
                e.onError(new Exception(response.message()));
            }
        } catch (Exception e1) {
            e.onError(e1);
        }
        e.onComplete();
    }
}
