package br.com.railsonrsa.runrun_it.ui.tasks.contracts;

import android.widget.ImageButton;

import br.com.railsonrsa.runrun_it.data.Task;
import br.com.railsonrsa.runrun_it.ui.base.BasePresenter;

/**
 * Created by railsonrsa on 29/06/17.
 */

public interface Presenter extends BasePresenter {
    void loadTasks();

    void starOrPauseTask(ImageButton taskStarOrStopButton, Task task);

    void finishTask(ImageButton taskFinishButton, Task task);
}
