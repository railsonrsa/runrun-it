package br.com.railsonrsa.runrun_it.model;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by railsonrsa on 29/06/17.
 */

public class RestFactory {

    private final int mWriteTime = 30;
    private final int mReadTime = 60;
    private String mAppKey = "aa538d3158d8f82cff1d97c364c87d22";
    private String mUserToken = "kuJea8L7teegM8wUz63y";
    private final String APP_KEY = "App-Key";
    private final String USER_TOKEN = "User-Token";
    private Retrofit mRetrofit;
    private HttpLoggingInterceptor mHttpLoggingInterceptor;
    private HttpLoggingInterceptor.Level mLoggingLevel = null;

    public RestFactory() {
        mRetrofit = null;
        mHttpLoggingInterceptor = new HttpLoggingInterceptor();
        if (mLoggingLevel == null) {
            setLoggingLevel(HttpLoggingInterceptor.Level.BASIC);
        }
    }

    public <API> API create(Class<API> service) {
        if (mRetrofit == null) {
            initRetrofit();
        }
        return mRetrofit.create(service);
    }

    private void initRetrofit() {

        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient().newBuilder()
                .writeTimeout(mWriteTime, TimeUnit.SECONDS)
                .readTimeout(mReadTime, TimeUnit.SECONDS);

        okHttpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder();
                builder.header(APP_KEY, mAppKey);
                builder.header(USER_TOKEN, mUserToken);
                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        });

        okHttpClientBuilder.addInterceptor(mHttpLoggingInterceptor);

        OkHttpClient okHttpClient = okHttpClientBuilder.build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl("https://secure.runrun.it/api/v1.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public void setLoggingLevel(HttpLoggingInterceptor.Level level) {
        mHttpLoggingInterceptor.setLevel(level);
    }
}
