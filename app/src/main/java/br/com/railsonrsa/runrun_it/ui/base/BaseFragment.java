package br.com.railsonrsa.runrun_it.ui.base;

import android.support.v4.app.Fragment;

/**
 * Created by railsonrsa on 29/06/17.
 */

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements BaseView<T> {
}
