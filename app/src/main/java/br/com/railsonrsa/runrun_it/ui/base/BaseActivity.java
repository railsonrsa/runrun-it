package br.com.railsonrsa.runrun_it.ui.base;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by railsonrsa on 29/06/17.
 */

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements BaseView<T> {
}
