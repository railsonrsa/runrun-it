package br.com.railsonrsa.runrun_it;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by railsonrsa on 29/06/17.
 */

public class RunrunApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
