package br.com.railsonrsa.runrun_it.ui.tasks;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.railsonrsa.runrun_it.R;
import br.com.railsonrsa.runrun_it.data.Task;
import br.com.railsonrsa.runrun_it.ui.tasks.adapters.TaskItemListener;
import br.com.railsonrsa.runrun_it.ui.tasks.adapters.TasksListAdapter;
import br.com.railsonrsa.runrun_it.ui.tasks.contracts.Presenter;
import br.com.railsonrsa.runrun_it.ui.tasks.contracts.TasksView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by railsonrsa on 29/06/17.
 */

public class TasksFragment extends TasksView {

    @BindView(R.id.tasks_recycler_view)
    RecyclerView tasksRecyclerView;

    private Presenter mPresenter;
    private List<Task> mData;
    private TasksListAdapter mTasksListAdapter;

    private static final String DATA = "mData";

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mData = (List<Task>) savedInstanceState.getSerializable(DATA);
            if (mData != null) {
                // UPDATE VIEW
            } else {
                mPresenter.start();
            }
        }
        mPresenter.start();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tasks, container, false);
        ButterKnife.bind(this, view);

        mData = new ArrayList<>();
        mTasksListAdapter = new TasksListAdapter(getActivity(), mData, new TaskItemListener() {
            @Override
            public void onClickPlayOrStopButton(ImageButton taskStarOrStopButton, Task task) {
                mPresenter.starOrPauseTask(taskStarOrStopButton, task);
            }

            @Override
            public void onClickFinishButton(ImageButton taskFinishButton, Task task) {
                mPresenter.finishTask(taskFinishButton, task);
            }
        });
        tasksRecyclerView.setAdapter(mTasksListAdapter);
        tasksRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(DATA, (Serializable) mData);
    }

    @Override
    public void setPresenter(Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void updateList(List<Task> tasks) {
        this.mData = tasks;
        mTasksListAdapter.addAll(tasks);
        mTasksListAdapter.notifyDataSetChanged();
    }

    @Override
    public void clearList() {
        tasksRecyclerView.removeAllViews();
        mTasksListAdapter.notifyItemRangeRemoved(0, mData.size());
        mTasksListAdapter.removeAll();
        mTasksListAdapter.notifyDataSetChanged();
    }
}
