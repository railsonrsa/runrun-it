package br.com.railsonrsa.runrun_it.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by railsonrsa on 29/06/17.
 */

public class Task extends RealmObject implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private int mId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("is_working_on")
    private boolean mIsWorkingOn;

    @SerializedName("queue_position")
    private int mQueuePosition;

    @SerializedName("client_name")
    private String mClientName;

    @SerializedName("project_name")
    private String mProjectName;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public boolean isWorkingOn() {
        return mIsWorkingOn;
    }

    public void setIsWorkingOn(boolean mIsWorkingOn) {
        this.mIsWorkingOn = mIsWorkingOn;
    }

    public int getQueuePosition() {
        return mQueuePosition;
    }

    public void setQueuePosition(int mQueuePosition) {
        this.mQueuePosition = mQueuePosition;
    }

    public String getClientName() {
        return mClientName;
    }

    public void setClientName(String mClientName) {
        this.mClientName = mClientName;
    }

    public String getProjectName() {
        return mProjectName;
    }

    public void setProjectName(String mProjectName) {
        this.mProjectName = mProjectName;
    }

    @Override
    public String toString() {
        return "Task{" +
                "mId=" + mId +
                ", mTitle='" + mTitle + '\'' +
                ", mIsWorkingOn=" + mIsWorkingOn +
                ", mQueuePosition=" + mQueuePosition +
                ", mClientName='" + mClientName + '\'' +
                ", mProjectName='" + mProjectName + '\'' +
                '}';
    }
}
