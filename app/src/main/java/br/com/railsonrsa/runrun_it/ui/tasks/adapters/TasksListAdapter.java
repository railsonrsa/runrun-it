package br.com.railsonrsa.runrun_it.ui.tasks.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import br.com.railsonrsa.runrun_it.R;
import br.com.railsonrsa.runrun_it.data.Task;

/**
 * Created by railsonrsa on 29/06/17.
 */

public class TasksListAdapter extends RecyclerView.Adapter<TasksListViewHolder> {

    private final LayoutInflater mLayoutInflater;
    private final Context mContext;
    private List<Task> mData;
    private final TaskItemListener mListener;

    public TasksListAdapter(Context context, List<Task> data, TaskItemListener listener) {
        mLayoutInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mData = data;
        this.mListener = listener;
    }

    @Override
    public TasksListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.task_item, parent, false);
        TasksListViewHolder viewHolder = new TasksListViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TasksListViewHolder holder, int position) {
        Task task = mData.get(position);

        // Highligh first item in queue
        if (task.getQueuePosition() == 1) {
            holder.taskPositionBackground.setBackgroundResource(R.drawable.circle_primary);
        }

        // Set queue position number
        holder.taskPositionNumber.setText(String.valueOf(task.getQueuePosition()));

        // Contact task id and task name
        StringBuilder sb = new StringBuilder()
                .append(task.getId())
                .append(" - ")
                .append(task.getTitle());
        holder.taskIdAndTitle.setText(sb.toString());

        // Verify item working status
        if (task.isWorkingOn()) {
            holder.taskStartStopButton.setSelected(true);
        }

        holder.bind(mListener, holder.taskStartStopButton, holder.taskFinishButton, task);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void addAll(List<Task> tasks) {
        this.mData.addAll(tasks);
    }

    public void removeAll() {
        this.mData.clear();
    }
}

class TasksListViewHolder extends RecyclerView.ViewHolder {

    FrameLayout taskPositionBackground;
    TextView taskPositionNumber;
    TextView taskIdAndTitle;
    ImageButton taskStartStopButton;
    ImageButton taskFinishButton;

    TasksListViewHolder(View itemView) {
        super(itemView);
        taskPositionBackground = (FrameLayout) itemView.findViewById(R.id.task_position_bg);
        taskPositionNumber = (TextView) itemView.findViewById(R.id.task_position_number);
        taskIdAndTitle = (TextView) itemView.findViewById(R.id.task_id_and_title);
        taskStartStopButton = (ImageButton) itemView.findViewById(R.id.task_start_stop);
        taskFinishButton = (ImageButton) itemView.findViewById(R.id.task_finish);
    }

    public void bind(final TaskItemListener mListener,
                     final ImageButton taskStartStopButton,
                     final ImageButton taskFinishButton,
                     final Task task) {

        taskStartStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickPlayOrStopButton(taskStartStopButton, task);
            }
        });
        taskFinishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickFinishButton(taskFinishButton, task);
            }
        });
    }
}
