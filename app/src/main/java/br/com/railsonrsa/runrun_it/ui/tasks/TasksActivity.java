package br.com.railsonrsa.runrun_it.ui.tasks;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import br.com.railsonrsa.runrun_it.R;
import br.com.railsonrsa.runrun_it.ui.tasks.contracts.ActivityView;
import br.com.railsonrsa.runrun_it.ui.tasks.contracts.Presenter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TasksActivity extends ActivityView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_content)
    FrameLayout activityContent;

    private Presenter mPresenter;
    private TasksFragment mTasksFragment;

    public static final String TASKS_FRAGMENT = "mTasksFragment";

    public TasksActivity() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            mTasksFragment = (TasksFragment)
                    getSupportFragmentManager().getFragment(savedInstanceState, TASKS_FRAGMENT);
        }

        if (mTasksFragment == null) {
            mTasksFragment = new TasksFragment();
        }

        new TasksPresenter(this, mTasksFragment);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_content, mTasksFragment, TASKS_FRAGMENT)
                .commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, TASKS_FRAGMENT, mTasksFragment);
    }

    @Override
    public void setPresenter(Presenter presenter) {
        this.mPresenter = presenter;
    }

}
