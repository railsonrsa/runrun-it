package br.com.railsonrsa.runrun_it.model.tasks;

import java.util.List;

import br.com.railsonrsa.runrun_it.data.Task;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by railsonrsa on 29/06/17.
 */

public interface TaskApi {
    @GET("tasks")
    Call<List<Task>> loadTasks(@Query("responsible_id") String responsibleId);

    @POST("tasks/{id}/play")
    Call<Task> playTask(@Path("id") int taskId);

    @POST("tasks/{id}/pause")
    Call<Task> pauseTask(@Path("id") int taskId);

    @POST("tasks/{id}/close")
    Call<Task> closeTask(@Path("id") int taskId);
}
